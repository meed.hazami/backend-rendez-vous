const express = require('express');
const Cours = require('./models/Cours');
const Coach = require('./models/Coaches');
const router = express.Router();

// Create a new cours
router.post('/api/cours', async (req, res) => {
    const { title, heur_deb, heur_fin, coachId, day } = req.body;

    try {
        const coach = await Coach.findById(coachId);
        if (!coach) {
            return res.status(404).json({ error: 'Coach not found' });
        }

        // Convert times to a comparable format
        const debTime = new Date(`1970-01-01T${heur_deb}:00Z`);
        const finTime = new Date(`1970-01-01T${heur_fin}:00Z`);

        if (debTime >= finTime) {
            return res.status(400).json({ error: 'The start hour must be before the end hour' });
        }

        const newCours = new Cours({
            title,
            heur_deb,
            heur_fin,
            coach: coachId,
            day
        });

        await newCours.save();
        res.status(201).json({ message: 'Cours created', cours: newCours });
    } catch (err) {
        console.error('Error creating cours:', err.message);
        res.status(500).send(err.message);
    }
});

// Get all cours
router.get('/api/cours', async (req, res) => {
    try {
        const cours = await Cours.find().populate('coach');
        res.status(200).json(cours);
    } catch (err) {
        res.status(500).send(err.message);
    }
});

// Get a single cours by ID
router.get('/api/cours/:id', async (req, res) => {
    const { id } = req.params;

    try {
        const cours = await Cours.findById(id).populate('coach');
        if (!cours) {
            return res.status(404).send('Cours not found');
        }
        res.status(200).json(cours);
    } catch (err) {
        res.status(500).send(err.message);
    }
});

// Update a cours by ID
router.put('/api/cours/:id', async (req, res) => {
    const { id } = req.params;
    const { title, heur_deb, heur_fin, coachId, day } = req.body;

    try {
        const cours = await Cours.findById(id);
        if (!cours) {
            return res.status(404).send('Cours not found');
        }

        if (coachId) {
            const coach = await Coach.findById(coachId);
            if (!coach) {
                return res.status(404).send('Coach not found');
            }
            cours.coach = coachId;
        }

        // Convert times to a comparable format
        const debTime = new Date(`1970-01-01T${heur_deb}:00Z`);
        const finTime = new Date(`1970-01-01T${heur_fin}:00Z`);

        if (debTime >= finTime) {
            return res.status(400).json({ error: 'The start hour must be before the end hour' });
        }

        cours.title = title || cours.title;
        cours.heur_deb = heur_deb || cours.heur_deb;
        cours.heur_fin = heur_fin || cours.heur_fin;
        cours.day = day || cours.day;

        await cours.save();
        res.status(200).json({ message: 'Cours updated', cours });
    } catch (err) {
        res.status(500).send(err.message);
    }
});

// Delete a cours by ID
router.delete('/api/cours/:id', async (req, res) => {
    const { id } = req.params;

    try {
        const cours = await Cours.findByIdAndDelete(id);
        if (!cours) {
            return res.status(404).send('Cours not found');
        }
        res.status(200).send('Cours deleted');
    } catch (err) {
        res.status(500).send(err.message);
    }
});

module.exports = router;
