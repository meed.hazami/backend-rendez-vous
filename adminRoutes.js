const express = require('express');
const { Disponibilite, AvailableHours } = require('./models/AvailableHours');

    const router = express.Router();

    router.get('/api/admin/get-available-hours', async (req, res) => {
        try {
            const availableHours = await AvailableHours.find().populate('date'); // Populate the date field

            // Check if availableHours is undefined or not an array
            if (!Array.isArray(availableHours)) {
                console.error('Available hours data is not an array:', availableHours);
                return res.status(500).json({ error: 'Internal Server Error' });
            }

            res.status(200).json(availableHours);
        } catch (error) {
            console.error('Error getting available hours:', error);
            res.status(500).json({ error: 'Internal Server Error' });
        }
    });

    router.post('/api/admin/set-available-hours', async (req, res) => {
        try {
            const selectedHoursWithIncrements = req.body.selectedHours || [];

            // Input validation
            if (!Array.isArray(selectedHoursWithIncrements)) {
                return res.status(400).json({ error: 'Invalid request body' });
            }

            for (const { date, hours, increments } of selectedHoursWithIncrements) {
                try {
                    // Find existing disponibilite or create a new entry
                    const existingDisponibilite = await Disponibilite.findOneAndUpdate(
                        { date },
                        { date },
                        { upsert: true, new: true, setDefaultsOnInsert: true }
                    );

                    // Create or update available hours
                    const existingAvailableHours = await AvailableHours.findOneAndUpdate(
                        { date: existingDisponibilite._id }, // Use the ID of the disponibilite
                        { hours },
                        { upsert: true, new: true, setDefaultsOnInsert: true }
                    );

                    // Update increments by pushing new increments
                    increments.forEach(({ hour, increment }) => {
                        const existingIncrementIndex = existingAvailableHours.increments.findIndex(existingIncrement => existingIncrement.hour === hour);

                        if (existingIncrementIndex !== -1) {
                            // Increment already exists, update the increment value
                            existingAvailableHours.increments[existingIncrementIndex].increment = increment;
                        } else {
                            // Increment doesn't exist, add a new entry
                            existingAvailableHours.increments.push({ hour, increment });
                        }
                    });

                    // Save the changes
                    await existingAvailableHours.save();

                    console.log('Updated Hours:', existingAvailableHours);
                    console.log('Updated Increments:', existingAvailableHours.increments);
                } catch (error) {
                    console.error('Error processing available hours for date:', date);
                    console.error(error);
                }
            }

            res.status(200).json({ message: 'Available hours set successfully' });
        } catch (error) {
            console.error('Error setting available hours:', error);
            res.status(500).json({ error: 'Internal Server Error' });
        }
    });

    router.delete('/api/admin/delete-past-dates', async (req, res) => {
        try {
            // Get current date as a string
            const currentDate = new Date().toISOString().split('T')[0];
    
            // Find the Disponibilite documents with dates before the current date
            const pastDisponibilites = await Disponibilite.find({ date: { $lt: currentDate } });
    
            // Get the IDs of the past Disponibilites
            const pastDisponibiliteIds = pastDisponibilites.map(disponibilite => disponibilite._id);
    
            // Delete all AvailableHours documents referencing the past Disponibilites
            await AvailableHours.deleteMany({ date: { $in: pastDisponibiliteIds } });
    
            // Respond with a JSON response
            res.status(200).json({ success: true, message: 'Past dates deleted successfully' });
        } catch (error) {
            console.error('Error deleting past dates:', error);
            res.status(500).json({ success: false, error: 'Internal Server Error' });
        }
    });
    
    function convertDateToDayName(dateString) {
        const daysOfWeek = ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'];
        const date = new Date(dateString);
        const dayIndex = date.getDay();
        return daysOfWeek[dayIndex];
      }
  
      router.get('/api/available-hours', async (req, res) => {
        const { date } = req.query;
        console.log('ok', { date });
      
        try {
          // Convert the date to the day name
          const dayName = convertDateToDayName(date);
      
          // Find the available hours data for the specific day name
          const availableHoursData = await AvailableHours.findOne({ date: dayName });
      
          // Log the value of availableHoursData and dayName for debugging
          console.log('Available Hours Data:', availableHoursData);
          console.log('Day Name:', dayName);
      
          // Check if availableHoursData is null or undefined before proceeding
          if (!availableHoursData) {
            console.error('No available hours data found for day:', date);
            return res.json([]);
          }
      
          // Fetch rendezvous for the specified day name
          const rendezvous = await Rendezvous.find({ date: dayName });
      
          // Map over each hour to determine its availability status
          const availableHoursWithStatus = availableHoursData.hours.map(hour => {
            // Find the increment data for the current hour
            const incrementData = availableHoursData.increments.find(item => item.hour === hour);
      
            // Count the number of rendezvous for the current hour
            const reservedCount = rendezvous.filter(rdv => rdv.hours.includes(hour)).length;
      
            // Determine if the hour is reserved based on the increment
            let reserved = false;
            if (incrementData) {
              reserved = reservedCount >= incrementData.increment;
            }
      
            return {
              hour,
              reserved
            };
          });
      
          res.json(availableHoursWithStatus);
        } catch (error) {
          console.error('Error fetching available hours:', error);
          res.status(500).send('Internal Server Error');
        }
      });
      const Rendezvous = require('./models/Rendezvous');

      router.get('/api/isAllHoursReserved', async (req, res) => {
        const { date } = req.query;
      
        try {
          // Convert the date to the expected format used in the database (e.g., "30-04-2024")
          const dayName = convertDateToDayName(date);
      
          // Find disponibilite document for the specific date
          const disponibilite = await Disponibilite.findOne({ date: dayName });
      
          if (!disponibilite) {
            return res.status(400).json({ error: 'Invalid date provided or no disponibilite found for the date.' });
          }
      
          // Ensure that disponibilite object has the _id property
          if (!disponibilite._id) {
            return res.status(400).json({ error: 'Missing or invalid _id data in disponibilite.' });
          }
      
          // Find available hours data using the _id from disponibilite
          const availableHoursData = await AvailableHours.findOne({ date: disponibilite._id });
      
          if (!availableHoursData) {
            return res.status(400).json({ error: 'No available hours data found for the date.' });
          }
      
          // Fetch rendezvous for the specific date
          const rendezvous = await Rendezvous.find({ date: date });
      
          // Check if all hours for the date are reserved within the increment limit
          const allHoursReserved = availableHoursData.hours.every(hour => {
            const incrementData = availableHoursData.increments.find(item => item.hour === hour);
            if (incrementData) {
              const reservedCount = rendezvous.filter(rendezvous => rendezvous.hours.includes(hour) && rendezvous.status).length;
              return reservedCount >= incrementData.increment;
            } else {
              // Handle the case where increment data is undefined
              return false;
            }
          });
      
          res.json(allHoursReserved);
        } catch (error) {
          console.error('Error checking if all hours are reserved:', error);
          res.status(500).send('Internal Server Error');
        }
      });
    
       
    module.exports = router;

