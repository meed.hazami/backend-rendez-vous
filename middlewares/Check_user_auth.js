

const authenticateToken = require('./middleware'); // Ensure this path is correct
const express = require('express');
const app = express();

const router = express.Router();



app.use(express.json());


router.get('/api/check-auth', authenticateToken, (req, res) => {
    // If the middleware calls next(), this route will execute
    res.json({ isAuthenticated: true });
  });
  module.exports = router;
