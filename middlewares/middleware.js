const jwt = require('jsonwebtoken');
const JWT_SECRET = "kfdsmdkfeoirpzeprkelmfdmlkhotko^ze46546;:,:;!l^m$";



const authenticateToken = (req, res, next) => {
    // Retrieve the token from the cookies sent with the request
    const token = req.cookies.token;

    // If no token is found, return an unauthorized error
    if (!token) {
        return res.status(403).json({ message: "A token is required for authentication" });
    }

    try {
        // Try to verify the token using the secret key
        const decoded = jwt.verify(token, JWT_SECRET);

        // Attach the decoded user to the request object
        req.user = decoded;

        // Proceed to the next middleware function
        next();
    } catch (error) {
        // If token verification fails, return an unauthorized error
        return res.status(401).json({ message: "Invalid Token" });
    }
};

module.exports = authenticateToken;
