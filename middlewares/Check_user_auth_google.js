

const express = require('express');
const app = express();
const { OAuth2Client } = require('google-auth-library');

const router = express.Router();


app.use(express.json());




const client = new OAuth2Client('496929401519-1jm69m43g8fh4tqv96j67h17oigjumbq.apps.googleusercontent.com');

router.get('/api/check-auth-google', async (req, res) => {
    const authHeader = req.headers.authorization;
    if (!authHeader) {
        return res.status(401).json({ isAuthenticated: false, message: 'No token provided' });
    }

    const token = authHeader.split(' ')[1]; // Assuming the Authorization header is "Bearer token"
    if (!token) {
        return res.status(401).json({ isAuthenticated: false, message: 'Bearer token not found' });
    }

    try {
        const ticket = await client.verifyIdToken({
            idToken: token,
            audience: '496929401519-1jm69m43g8fh4tqv96j67h17oigjumbq.apps.googleusercontent.com',
        });
        const payload = ticket.getPayload();

        // You might want to do additional checks or store user info
        res.json({ isAuthenticated: true, user: payload });
    } catch (error) {
        console.error('Error verifying Google token:', error);
        res.status(403).json({ isAuthenticated: false, message: 'Invalid token' });
    }
});

module.exports = router;



module.exports=router
