const { OAuth2Client } = require('google-auth-library');
const client = new OAuth2Client('496929401519-1jm69m43g8fh4tqv96j67h17oigjumbq.apps.googleusercontent.com');

async function verifyGoogleToken(req, res, next) {
    const { token } = req.cookies;
    if (!token) {
        console.error("No token provided");
        return res.status(400).send("No token provided.");
    }

    try {
        const ticket = await client.verifyIdToken({
            idToken: token,
            audience:'496929401519-1jm69m43g8fh4tqv96j67h17oigjumbq.apps.googleusercontent.com',
        });
        const payload = ticket.getPayload();

        req.user = {
            id: payload['sub'],
            name: payload['name'],
            email: payload['email'],
            picture: payload['picture']
        };

        next();
    } catch (error) {
        console.error("Google token verification failed:", error);
        return res.status(401).send("Invalid Google token.");
    }
}

module.exports=verifyGoogleToken