const mongoose = require('mongoose');
const AutoInc = require('mongoose-sequence')(mongoose);

const coachSchema = new mongoose.Schema({
  id_coach:{type:Number},
  name: {
    type: String,
    required: true
  },
  type_sport:{
    type:String,
    enum:['cardio','Yoga',"Crossfit"],
    required:true
  },
  status: {
    type: String,
    enum: ['disponible', 'non disponible'],
    default: 'disponible'
  },
  sexe:{
    type:String,
    enum:['Homme','Femme'],
    required:true 
  }
});
coachSchema.plugin(AutoInc, { inc_field: 'id_coach' });


const Coach = mongoose.model('Coach', coachSchema);

module.exports = Coach;
