const mongoose = require('mongoose');
const AutoInc = require('mongoose-sequence')(mongoose);

const SchemaClient = new mongoose.Schema({
    id_client:{type:Number

    },
    username: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    confirm_password: {
        type: String,
        required: true
    },
    plainPassword: {
        type: String,
        select: false 
    },
  
        avatar:{
            type:String,

        },


   
    role:{
        type:String,
        default:'client'
    }
}, { timestamps: true });
// Apply the auto-increment plugin to id_users
SchemaClient.plugin(AutoInc, { inc_field: 'id_client' });

module.exports = mongoose.model('Client', SchemaClient);
