const mongoose = require('mongoose');

const PackSchema = new mongoose.Schema({
    ref:{
        type:String,
        required:true
    },
  name: {
    type: String,
    required: true
  },
  Prix_pack:{

    type:String,
    required:true
  },
  description:{

    type:String,
    required:true
  },
  Name_coach: { // Reference to the NameCoach model
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Coach' // This should be the name of the related Mongoose model
  },

  img_pack:{
    type:String,
  }
  
});

const Pack = mongoose.model('Pack', PackSchema);

module.exports = Pack;
