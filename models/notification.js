const mongoose = require('mongoose');

const notificationSchema = new mongoose.Schema({
    rendezvous: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'rendezvous' // Reference to the Rendezvous model
    },
    visiteur: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Visiteur' // Reference to the Visiteur model
    },
    read: {
        type: Boolean,
        default: false // Notification is unread by default
    },
    createdAt: {
        type: Date,
        default: Date.now // Timestamp of when the notification was created
    }
});

const Notification = mongoose.model('Notification', notificationSchema);
module.exports = Notification;
