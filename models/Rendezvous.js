const mongoose = require('mongoose');

const rendezvousSchema = new mongoose.Schema({
    date: String,
    hours: [String], // Renamed to French 'heures'
    visiteur: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Visiteur' // Reference to the Visiteur model
    },
        coach: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Coach', // Reference to the Coach model
        default: null // Default to null if not provided
    },
    coachName: {
        type: String,
        default: 'Default Coach' // Default coach name
    },
    status: {
        type: Boolean,
        default: false
    },
    description: String,
    source:{
                type: String,

        enum:['facebook','instagram','tiktok','google']
    }
});

const Rendezvous = mongoose.model('Rendezvous', rendezvousSchema);
module.exports = Rendezvous;
