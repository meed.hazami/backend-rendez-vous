const mongoose = require('mongoose');

const visiteurSchema = new mongoose.Schema({
    name:{
        type:String,
        required:true
    },
    lastName:{
        type:String,
        required:true
    },

    mail:{
        type:String,
        required:true


    },
    phoneNumber:{type:Number,
        required:true
    },
    age:{
        type:Number,
        required:true

    },
    genre:{
        enum:['homme','femme']
    },
   
});

const Visiteur = mongoose.model('Visiteur', visiteurSchema);
module.exports = Visiteur; // Assurez-vous d'exporter le modèle Visiteur
