const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const AutoInc = require('mongoose-sequence')(mongoose);

const UserSchema = new Schema({
    id_users: { type: Number },
    username: { type: String, required: true },
    email: { type: String, required: true, unique: true },
    password: { type: String, required: true },
    Confirm_password: { type: String },
    role: { type: String, enum: ['admin', 'user'], default: 'user' },
    phone: { type: String, required: true },
    emailToken: { type: String },
    isVerified: { type: Boolean, default: false },
    createdAt: { type: Date, default: Date.now }
});

// Apply the auto-increment plugin to id_users
UserSchema.plugin(AutoInc, { inc_field: 'id_users' });

const User = mongoose.model('User', UserSchema);

module.exports = User;
