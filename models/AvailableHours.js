const mongoose = require('mongoose');
for (const modelName in mongoose.models) {
  delete mongoose.models[modelName];
}
const disponibilite = new mongoose.Schema({
  date: { type: String, unique: true }
});

const availableHoursSchema = new mongoose.Schema({
  date: { type: mongoose.Schema.Types.ObjectId, ref: 'Disponibilite' }, 
  hours: [String],
  increments: [{
    hour: String,
    increment: Number,
}],
});

// Check if the model already exists before creating it
const Disponibilite = mongoose.models.Disponibilite || mongoose.model('Disponibilite', disponibilite);
const AvailableHours = mongoose.models.AvailableHours || mongoose.model('AvailableHours', availableHoursSchema);

module.exports = { Disponibilite, AvailableHours }; 