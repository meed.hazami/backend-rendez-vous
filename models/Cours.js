const mongoose = require('mongoose');

const coursSchema = new mongoose.Schema({
  title: {
    type: String,
    required: true
  },
  heur_deb: {
    type: String,
    required: true
  },
  heur_fin: {
    type: String,
    required: true
  },
  coach: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Coach',
    required: true
  },
  day: {
    type: String,
    enum: ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'],
    required: true
  }
});

const Cours = mongoose.model('Cours', coursSchema);

module.exports = Cours;
