const express = require('express');
const Client = require('./models/Clients');
const router = express.Router();
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const crypto = require('crypto');
const cloudinary = require('cloudinary').v2; 
const JWT_SECRET_CLIENT = '"ç-é²123;:,ùlm,opôraî^zeopazofklkmgjkgjfozezrk^zldp^lplqspdlmlfkùkjqosjazp"'; // Replace with your actual secret
const multer = require('multer'); // Import multer

// Create Client
router.post('/api/clients', async (req, res) => {
    const { username, email } = req.body;

    try {
        const findClient = await Client.findOne({ email });
        if (findClient) {
            return res.status(400).send('Email already exists!');
        }

        // Generate a random password
        const plainPassword = crypto.randomBytes(8).toString('hex');
        console.log('Generated plain password:', plainPassword);

        // Hash the password
        const hashedPassword = await bcrypt.hash(plainPassword, 12);
        console.log('Hashed password:', hashedPassword);

        const newClient = new Client({
            username,
            email,
            password: hashedPassword,
            confirm_password: hashedPassword,
            plainPassword
        });

        await newClient.save();

        // Generate JWT token
        const token = jwt.sign({ id: newClient._id }, JWT_SECRET_CLIENT, { expiresIn: '1h' });

        res.status(201).json({ message: 'Client created', client: newClient, plainPassword });
    } catch (err) {
        console.error('Error creating client:', err);
        res.status(500).send(err.message);
    }
});

// Read Clients
router.get('/api/clients', async (req, res) => {
    const { page = 1, limit = 4 } = req.query;
    try {
        const clients = await Client.find()
            .limit(limit * 1)
            .skip((page - 1) * limit)
            .exec();
        const count = await Client.countDocuments();
        res.json({
            clients,
            totalPages: Math.ceil(count / limit),
            currentPage: page
        });
    } catch (err) {
        res.status(500).send(err.message);
    }
});

// One Client
router.get('/api/clients/:id', async (req, res) => {
    const { id } = req.params;
    try {
        const client = await Client.findById(id).select('+plainPassword');
        if (client) {
            res.status(200).json({ client, plainPassword: client.plainPassword });
        } else {
            res.status(404).json({ error: 'Client not found!' });
        }
    } catch (err) {
        res.status(500).send(err.message);
    }
});

// Update Client
router.put('/api/clients/:id', async (req, res) => {
    const { id } = req.params;
    const { username, email, avatar } = req.body;

    const updates = {};
    if (username) updates.username = username;
    if (email) updates.email = email;
    if (avatar) updates.avatar = avatar;

    try {
        const client = await Client.findByIdAndUpdate(id, updates, { new: true });
        if (!client) {
            return res.status(404).send('Client not found');
        }
        res.status(200).send(client);
    } catch (err) {
        res.status(500).send(err.message);
    }
});
// Delete Client
router.delete('/api/clients/:id', async (req, res) => {
    const { id } = req.params;

    try {
        const client = await Client.findByIdAndDelete(id);
        if (!client) {
            return res.status(404).send('Client not found');
        }
        res.status(200).send('Client deleted');
    } catch (err) {
        res.status(500).send(err.message);
    }
});


// Configure Cloudinary
cloudinary.config({ 
    cloud_name: 'dr9pc5rcx', 
    api_key: '662417328642619', 
    api_secret: 'vtoGD85jiayhvAZ_TCD9G85dq-s' 
  });
  
  
// Multer setup for file uploads
const storage = multer.memoryStorage();
const upload = multer({ storage });

  router.post('/api/upload-avatar', upload.single('avatar'), async (req, res) => {
    try {
      const result = await cloudinary.uploader.upload_stream((error, result) => {
        if (error) {
          return res.status(500).send(error);
        }
        res.status(200).send({ url: result.secure_url });
      }).end(req.file.buffer);
    } catch (error) {
      console.error('Error uploading avatar:', error);
      res.status(500).send('Error uploading avatar');
    }
  });

module.exports = router;
