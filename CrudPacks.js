const express = require('express');
const router = express.Router();
const multer = require('multer'); // Import multer
const Pack = require('./models/Pack');
const coach = require('./models/Coaches');
const cloudinary = require('cloudinary').v2; // Import Cloudinary SDK

// Configure Cloudinary with your credentials
cloudinary.config({ 
  cloud_name: 'dr9pc5rcx', 
  api_key: '662417328642619', 
  api_secret: 'vtoGD85jiayhvAZ_TCD9G85dq-s' 
});

const upload = multer({ dest: 'uploads/' });

router.post('/api/packs', upload.single('image'), async (req, res) => {
  const { ref, name, Prix_pack, description, Name_coach } = req.body;
  try {
    if (!req.file) {
      return res.status(400).json({ message: "No image file provided" });
    }
    
    const cloudinaryResponse = await cloudinary.uploader.upload(req.file.path);
    const img_packUrl = cloudinaryResponse.secure_url;

    const newPack = new Pack({
      ref,
      name,
      Prix_pack,
      description,
      Name_coach, // Assuming Name_coach is the ObjectId of the coach
      img_pack: img_packUrl
    });

    const savedPack = await newPack.save();
    res.json(savedPack);
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
});

router.get('/api/packs', async (req, res) => {
  try {
    const packs = await Pack.find().populate('Name_coach', 'name');
    res.json(packs);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
});


// Read a specific Pack by ID
router.get('/api/packs/:id', async (req, res) => {
  try {
    const id = req.params.id.trim(); 
    const pack = await Pack.findById(id);
    if (pack) {
      res.json(pack);
    } else {
      res.status(404).json({ message: "Pack not found" });
    }
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
});

// Update a Pack
router.put('/api/packs/:id', upload.single('image'), async (req, res) => {
  try {
    const { ref, name, Prix_pack, description, Name_coach } = req.body;
    let img_packUrl;

    // Check if a new image file is provided
    if (req.file) {
      const cloudinaryResponse = await cloudinary.uploader.upload(req.file.path);
      img_packUrl = cloudinaryResponse.secure_url;
    }

    const updatedPack = await Pack.findByIdAndUpdate(
      req.params.id,
      {
        ref,
        name,
        Prix_pack,
        description,
        Name_coach,
        ...(img_packUrl && { img_pack: img_packUrl }) // Update image URL if a new image is provided
      },
      { new: true }
    );

    if (updatedPack) {
      res.json(updatedPack);
    } else {
      res.status(404).json({ message: "Pack not found" });
    }
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
});

// Delete a Pack
router.delete('/api/packs/:id', async (req, res) => {
  try {
    const deletedPack = await Pack.findByIdAndDelete(req.params.id);
    if (deletedPack) {
      res.json(deletedPack);
    } else {
      res.status(404).json({ message: "Pack not found" });
    }
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
});

module.exports = router;
