const express = require('express');
const { Disponibilite, AvailableHours } = require('./models/AvailableHours');
const Rendezvous = require('./models/Rendezvous');
const Visiteur = require('./models/Visiteur');
const Coach = require('./models/Coaches'); // Replace './Coach' with the correct path to your Coach model file
const Notification = require('./models/notification');

const resr = express.Router();

function convertDateToDayName(dateString) {
  const daysOfWeek = ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'];
  const date = new Date(dateString);
  const dayIndex = date.getDay();
  return daysOfWeek[dayIndex];
}

resr.get('/api/reservation-count', async (req, res) => {
  const { date } = req.query;

  try { 
    // Convert the date to the day name
    // Fetching data based on the day of the week might not be precise enough for booking dates and times.
const dayName = convertDateToDayName(date);
const disponibilite = await Disponibilite.findOne({ date: dayName });

    if (!disponibilite) {
      console.error('No available hours data found for day:', dayName);
      return res.json([]);
    }

    // Find available hours data for the specific day
    const availableHoursData = await AvailableHours.findOne({ date: disponibilite._id });

    if (!availableHoursData) {
      console.error('No available hours data found for day:', dayName);
      return res.json([]);
    }

    // Fetch rendezvous for the specific day
    const rendezvous = await Rendezvous.find({ date });

    // Map each hour to determine its availability status
    const availableHoursWithStatus = availableHoursData.hours.map(hour => {
      // Find the increment data for the current hour
      const incrementData = availableHoursData.increments.find(item => item.hour === hour);

      // Count the number of rendezvous for the current hour
      const reservedCount = rendezvous.filter(rendezvous => rendezvous.hours.includes(hour) && rendezvous.status).length;

      // Determine if the hour is reserved based on the increment
      let reserved = 0;
      if (incrementData) {
        reserved = incrementData.increment - reservedCount;
      }

      return {
        hour,
        reserved
      };
    });

    res.json(availableHoursWithStatus);
  } catch (error) {
    console.error('Error fetching available hours:', error);
    res.status(500).send('Internal Server Error');
  }
});




const isAuthenticated = (req, res, next) => {
  const token = req.headers.authorization?.split(' ')[1];  // Assuming token is sent as "Bearer <token>"
  if (!token) {
    return res.status(401).json({ error: 'No token provided' });
  }

  jwt.verify(token, process.env.JWT_SECRET, (err, decoded) => {
    if (err) {
      return res.status(401).json({ error: 'Failed to authenticate token' });
    }
    req.user = decoded; // Add decoded user to request object
    next();
  });
};







resr.post('/api/create-reservation', async (req, res) => {
  try {
    const { date, hours, name, lastName, mail, phoneNumber, age, description, source } = req.body || {};

    if (!date || !hours || !name || !lastName || !mail  ) {
      return res.status(400).json({ error: 'Invalid request body' });
    }
    
    if(phoneNumber.toString().length!=8){

      return res.status(404).json({error:'Check your phone number !'})
    }

    // Check if the visitor exists
    let visitor = await Visiteur.findOne({ mail });
    if (!visitor) {
      visitor = new Visiteur({
        name,
        lastName,
        mail,
        phoneNumber,
        age,
      });
      await visitor.save();
    }

    // Check if the visitor already has a reservation for the same date and time
    const existingReservation = await Rendezvous.findOne({ visiteur: visitor._id });
    if (existingReservation) {
      return res.status(400).json({ error: 'You already make a reservation !' });
    }

    // Create the appointment
    const rendezvous = new Rendezvous({
      date,
      hours: hours,
      visiteur: visitor._id, // Associate the visitor with the appointment
      description,
      source
    });

    await rendezvous.save();

    // Create a notification for the reservation
    const notification = new Notification({
      rendezvous: rendezvous._id,
      visiteur: visitor._id
    });
    await notification.save();

    res.status(201).json({ message: 'Rendez-vous created successfully', rendezvous });
  } catch (error) {
    console.error('Error creating rendez-vous:', error);
    res.status(500).json({ error: 'Internal Server Error' });
  }
});



resr.put('/api/accept-reservation/:rendezvousId', async (req, res) => {
  try {
    const { rendezvousId } = req.params;
    const { coachId } = req.body;

    const rendezvous = await Rendezvous.findById(rendezvousId);
    if (!rendezvous) {
      return res.status(404).json({ error: 'Rendezvous not found' });
    }

    // Update status to true
    rendezvous.status = true;

    // Fetch the selected coach from the database
    const coach = await Coach.findById(coachId);
    if (!coach) {
      return res.status(404).json({ error: 'Coach not found' });
    }

    // Update the rendezvous with the selected coach's ID and name
    rendezvous.coach = coachId;
    rendezvous.coachName = coach.name;

    await rendezvous.save();

    // Retrieve the visitor's email address using the visitor's ID
    const visitorWithEmail = await Rendezvous.findById(rendezvous._id).populate('visiteur');
    const visitorEmail = visitorWithEmail.visiteur.mail;

    // Define email options with the visitor's email address
   

    res.json({ message: 'Rendezvous accepted successfully', rendezvous });
  } catch (error) {
    console.error('Error accepting rendezvous:', error);
    res.status(500).json({ error: 'Internal Server Error' });
  }
});


  // Backend code (Node.js with Express)
  resr.delete('/api/delete-rendezvous/:rendezvousId', async (req, res) => {
    try {
      const { rendezvousId } = req.params;
  
      // Find the rendezvous by ID and delete it
      await Rendezvous.findByIdAndDelete(rendezvousId);
  
      res.json({ message: 'Rendezvous deleted successfully' });
    } catch (error) {
      console.error('Error deleting rendezvous:', error);
      res.status(500).json({ error: 'Internal Server Error' });
    }
  });
    // Define API endpoint to fetch reservations
    resr.get('/api/rendezvous', async (req, res) => {
      try {
        // Fetch all rendezvous from the database
        const rendezvous = await Rendezvous.find();
        res.json(rendezvous);
      } catch (error) {
        console.error('Error fetching rendezvous:', error);
        res.status(500).send('Internal Server Error');
      }
    });
    // Define API endpoint to fetch a rendezvous by ID
resr.get('/api/rendezvousByID/:id', async (req, res) => {
  try {
    // Fetch the rendezvous by ID from the database
    const rendezvous = await Rendezvous.findById(req.params.id);
    
    // If rendezvous is not found, return a 404 response
    if (!rendezvous) {
      return res.status(404).json({ message: 'Rendezvous not found' });
    }

    // If rendezvous is found, return it
    res.json(rendezvous);
  } catch (error) {
    console.error('Error fetching rendezvous by ID:', error);
    res.status(500).send('Internal Server Error');
  }
});

resr.get('/api/AcceptedReservations', async (req, res) => {
  try {
    // Fetch only accepted reservations from the database
    const rendezvous = await Rendezvous.find({ status: true }).populate('visiteur');
    res.json(rendezvous);
  } catch (error) {
    console.error('Error fetching accepted reservations:', error);
    res.status(500).send('Internal Server Error');
  }
});

module.exports = resr;
