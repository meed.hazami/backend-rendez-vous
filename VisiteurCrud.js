const express = require('express');
const Visiteur = require('./models/Visiteur');
const router = express.Router();

// Create a Visiteur
router.post('/api/visiteurs', async (req, res) => {
  try {
    const { name, lastName, mail, phoneNumber, age,genre } = req.body;
    const newVisiteur = new Visiteur({
      name,
      lastName,
      mail,
      phoneNumber,
      age,
      genre
    });
    const savedVisiteur = await newVisiteur.save();
    res.json(savedVisiteur);
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
});

// Read all Visiteurs
router.get('/api/visiteurs', async (req, res) => {
  try {
    const visiteurs = await Visiteur.find();
    res.json(visiteurs);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
});

// Read a specific Visiteur by ID
router.get('/api/visiteurs/:id', async (req, res) => {
  try {
    const visiteur = await Visiteur.findById(req.params.id);
    if (visiteur) {
      res.json(visiteur);
    } else {
      res.status(404).json({ message: "Visiteur not found" });
    }
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
});

// Update a Visiteur
router.put('/api/visiteurs/:id', async (req, res) => {
  try {
    const { name, lastName, mail, phoneNumber, age } = req.body;
    const updatedVisiteur = await Visiteur.findByIdAndUpdate(req.params.id, {
      name,
      lastName,
      mail,
      phoneNumber,
      age
    }, { new: true });
    if (updatedVisiteur) {
      res.json(updatedVisiteur);
    } else {
      res.status(404).json({ message: "Visiteur not found" });
    }
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
});

// Delete a Visiteur
router.delete('/api/visiteurs/:id', async (req, res) => {
  try {
    const deletedVisiteur = await Visiteur.findByIdAndDelete(req.params.id);
    if (deletedVisiteur) {
      res.json(deletedVisiteur);
    } else {
      res.status(404).json({ message: "Visiteur not found" });
    }
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
});

module.exports = router;
