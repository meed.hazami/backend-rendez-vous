const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const cors = require('cors');
const jwt = require('jsonwebtoken');
const cookieParser = require('cookie-parser');

const ReservationCrud = require('./ReservationCrud');
const visiteurCrud = require('./VisiteurCrud');
const PackCrud = require('./CrudPacks');
const notificationCrud=require('./notificationCrud')
const ClientRoutes=require('./CrudClient')
const UserRoutes = require('./CrudUser');
const path = require('path');
const CoursRoute=require('./crudCours')
const SigninRoute = require('./Authentification/SignInRoute');
const SigninClientRoute=require('./Authentification/SigninClientRoute')
const SignUpRoute = require('./Authentification/signupRoute');
const adminRoutes=require('./adminRoutes');
const CrudCoach=require('./CrudCoaches')
const LoginWithGoogle = require('./Authentification/LoginWithGoogle');
const Logout = require('./Authentification/logout');
const check_user_auth_google = require('./middlewares/Check_user_auth_google');
const Check_user_auth = require('./middlewares/Check_user_auth');
const resetPassowrd=require('./Authentification/ResetPassowrd')
//statistics

const coachesInfluence=require('./Statistics/CoachInfluence')
const CountCoach=require('./Statistics/CountCoach')
const countRendezVousPerDay=require('./Statistics/CountRendezVousPerDay')
const mostCoachPerWeek=require('./Statistics/mostCoachPerWeek')
const RendezVousParStatus=require('./Statistics/RendezVousParStatus')
const SourcesPie=require('./Statistics/CountSocialMediaTheMost')
const MostCoachPerMonth = require('./Statistics/MostCoachPerMonth'); // Import the new route
const AgeVisitor=require('./Statistics/VisitorsAge')
const CountClient=require('./Statistics/CountClients')
const CountVisiteur=require('./Statistics/CountVisitors')
const AgeCountVisitor=require('./Statistics/CountAgeVisitors')
//authentification
const vemail=require('./Authentification/verifyEmail')
require('dotenv').config();
const app = express();
const PORT = process.env.PORT || 3008;
const JWT_SECRET = process.env.JWT_SECRET;

// Connect to MongoDB
mongoose.connect(process.env.DB, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});

// Middleware
app.use(express.json());
app.use(bodyParser.json());
app.use(cookieParser());

// CORS settings
const corsOptions = {
  origin: true,
  credentials: true, // to allow cookies to be sent
  allowedHeaders: ['Authorization', 'Content-Type', 'X-Requested-With', 'Accept'],
};
app.use(cors(corsOptions)); // Apply CORS with the options

// Authentification Routes
app.use('/', SigninRoute);
app.use('/',SigninClientRoute)
app.use('/', LoginWithGoogle);
app.use('/', Logout);
app.use('/',resetPassowrd)

app.use(SignUpRoute);
app.use(vemail);

// Serve the React application


//MiddleWares 
app.use('/', check_user_auth_google);
app.use('/', Check_user_auth);
//Routes
app.use('/',CoursRoute)
app.use('/', ReservationCrud);
app.use('/', visiteurCrud);
app.use('/',CrudCoach)
app.use('/', PackCrud);
app.use('/', UserRoutes);
app.use('/',notificationCrud)
app.use('/',adminRoutes)
app.use('/',ClientRoutes)
//statisics
app.use('/',coachesInfluence)
app.use('/',CountCoach)
app.use('/',countRendezVousPerDay)
app.use('/',MostCoachPerMonth)
app.use('/',mostCoachPerWeek)
app.use('/',RendezVousParStatus)
app.use('/',SourcesPie)
app.use('/',AgeVisitor)
app.use('/',CountClient)
app.use('/',CountVisiteur)
app.use('/',AgeCountVisitor)
//Authentification
app.listen(PORT, () => {
  console.log(`Server is running on ${PORT}`);
});
