const express = require('express');
const { OAuth2Client } = require('google-auth-library');


const app = express();
app.use(express.json());

const client = new OAuth2Client("496929401519-1jm69m43g8fh4tqv96j67h17oigjumbq.apps.googleusercontent.com");

// Create router
const router = express.Router();


router.post('/api/auth/google', async (req, res) => {
  // Extract the token from request body
  const { token } = req.body;
  console.log('Token received:', token);

  if (!token) {
    return res.status(400).json({ success: false, message: 'Token not provided' });
  }

  try {
    // Verify the ID token using Google's OAuth2Client
    const dataclient = await client.verifyIdToken({
      idToken: token,
      audience: "496929401519-1jm69m43g8fh4tqv96j67h17oigjumbq.apps.googleusercontent.com", // Ensure this is the correct client ID
    });

    // Get the user payload from the ticket
    const payload = dataclient.getPayload();
    console.log(payload)

    res.json({
        success: true,
        message: 'Token is valid and user data received',
        user: {
          email: payload.email,
          familyName: payload.family_name,  // Include the family name
          givenName: payload.given_name,
          picture: payload.picture
          

        }
      });
    } catch (error) {
      console.error('Error verifying Google token:', error);
      res.status(401).json({ success: false, message: 'Invalid token' });
    }
  });

// Export the router
module.exports = router;

