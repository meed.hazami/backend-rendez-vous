const express = require('express');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt'); // Ensure bcryptjs is installed for password hashing
const User = require('../models/user');
const router = express.Router();

const JWT_SECRET = "kfdsmdkfeoirpzeprkelmfdmlkhotko^ze46546;:,:;!l^m$";

router.post('/api/resetPassword', async (req, res) => {
    const { email } = req.body;
    console.log(email)
    try {
        const user = await User.findOne({ email });
        if (!user) {
            return res.json({ status: "User not Exists!" });
        }

        const secretCode = JWT_SECRET + user.password;
        const token = jwt.sign({ email: user.email, id: user._id }, secretCode, {
            expiresIn: "1m"
        });
        const link = `http://localhost:3000/resetPassword/${user._id}/${token}`;
        console.log(link);
        res.json({ status: "The link has been sended !", link });
    } catch (error) {
        res.status(500).json({ error: "Internal server error" });
    }
});



router.post('/api/updatePassword/:id/:token', async (req, res) => {
    const { id, token } = req.params;
    const { newPassword } = req.body;

    try {
        const user = await User.findOne({ _id: id });
        if (!user) {
            return res.status(404).json({ status: "User not found" });
        }

        const secretCode = JWT_SECRET + user.password;
        jwt.verify(token, secretCode);

        const salt = await bcrypt.genSalt(10);
        const hashedPassword = await bcrypt.hash(newPassword, salt);

        user.password = hashedPassword;
        user.Confirm_password=newPassword;
        await user.save();

        res.json({ status: "Password updated successfully" });
    } catch (error) {
        if (error.name === 'JsonWebTokenError') {
            return res.status(401).json({ status: "Invalid or expired token" });
        }
        res.status(500).json({ status: "Error updating password" });
    }
});

module.exports = router;
