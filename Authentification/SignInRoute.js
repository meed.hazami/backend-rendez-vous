const express = require('express');
const router = express.Router();
const User = require('../models/user');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const JWT_SECRET = "kfdsmdkfeoirpzeprkelmfdmlkhotko^ze46546;:,:;!l^m$";

router.post("/api/signin", async (req, res) => {
    try {
        const { email, password } = req.body;

        if (!email || !password) {
            return res.status(400).json({ error: "Email or password is empty!" });
        }

        const findUser = await User.findOne({ email });
        
        if (!findUser) {
            return res.status(404).json({ error: "User not found" });
        }

        // Check email verification status only for role 'user'
        if (findUser.role === 'user' && !findUser.isVerified) {
            return res.status(400).json({ error: 'Email not verified' });
        }

        const matchPassword = await bcrypt.compare(password, findUser.password);
        if (!matchPassword) {
            return res.status(401).json({ error: "Wrong email or password" });
        }

        const token = jwt.sign({ id: findUser._id.toString(), username: findUser.username, role: findUser.role }, JWT_SECRET);
        res.cookie('token', token, { httpOnly: true, secure: true }).json({ findUser, token });
    } catch (error) {
        console.error("Sign-in error:", error);
        res.status(500).json({ error: "An error occurred while signing in" });
    }
});

module.exports = router;
