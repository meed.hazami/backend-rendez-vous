const User = require('../models/user');
const express = require('express');

const router = express.Router();

router.get('/api/verify-email', async (request, response) => {
    const { token } = request.query;

    try {
        const user = await User.findOne({ emailToken: token });

        if (!user) {
            return response.status(400).json({ error: 'Invalid token!' });
        }

        user.emailToken = null;
        user.isVerified = true;

        await user.save();

        return response.status(200).json({ message: 'Email verified successfully!' });
    } catch (err) {
        console.error('Error during email verification:', err);
        return response.status(500).json({ message: err.message });
    }
});

module.exports = router;
