
const express = require('express');

const router=express.Router()
router.get('/api/logout',(req,res)=>{

    res.clearCookie('token'); // Ensure 'authToken' matches the name of your auth cookie
  res.status(200).json({ message: 'Logged out successfully' });
})


module.exports=router