const express = require('express');
const Client = require('../models/Clients');
const router = express.Router();
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const JWT_SECRET_CLIENT = '"ç-é²123;:,ùlm,opôraî^zeopazofklkmgjkgjfozezrk^zldp^lplqspdlmlfkùkjqosjazp"'; // Replace with your actual secret

// Sign In Route
router.post('/api/clients/signin', async (req, res) => {
    const { usernameOrEmail, password } = req.body;

    console.log('Sign-in request received:', req.body); // Log incoming request

    try {
        // Find the client by email or username
        const client = await Client.findOne({ 
            $or: [{ email: usernameOrEmail }, { username: usernameOrEmail }] 
        });
        
        if (!client) {
            console.log('Client not found for usernameOrEmail:', usernameOrEmail);
            return res.status(404).send('Client not found');
        }

        // Compare the provided password with the hashed password
        const isMatch = await bcrypt.compare(password, client.password);
        console.log('Password comparison result:', isMatch); // Log comparison result

        if (!isMatch) {
            return res.status(400).send('Invalid credentials');
        }

        // Generate JWT token
        const token = jwt.sign({ id: client._id }, JWT_SECRET_CLIENT, { expiresIn: '1h' });

        console.log('Sign-in successful for client:', client);

        res.status(200).json({ clientId: client._id, username: client.username, token });
    } catch (err) {
        console.error('Error during sign-in:', err);
        res.status(500).send(err.message);
    }
});

module.exports = router;
