const bcrypt = require('bcrypt');
const User = require('../models/user');
const express = require('express');
const crypto = require('crypto');

const router = express.Router();

router.post('/api/signup', async (request, response) => {
    const { username, email, password, phone, Confirm_password } = request.body;

    try {
        if (!username || !email || !password || !phone || !Confirm_password) {
            return response.status(400).json({ error: "All inputs are required!" });
        }

        if (password !== Confirm_password) {
            return response.status(400).json({ error: 'Password and confirm password should be the same!' });
        }

        const findUser = await User.findOne({ email });
        if (findUser) {
            return response.status(400).json({ error: 'Email is taken!' });
        } else {
            const h_password = await bcrypt.hash(password, 12);
            const emailToken = crypto.randomBytes(64).toString('hex');
            const newUser = new User({ username, email, phone, password: h_password, emailToken });

            await newUser.save();

            return response.status(200).json({ message: "User created, please verify your email.", emailToken });
        }
    } catch (err) {
        console.error('Error during user signup:', err);
        return response.status(500).json({ message: err.message });
    }
});

module.exports = router;
