const express = require('express');
const router = express.Router();
const Rendezvous = require('../models/Rendezvous');

router.get('/api/stats/most-clients-per-week', async (req, res) => {
  try {
    const today = new Date();
    console.log(today)
    const oneWeekAgo = new Date(today.getFullYear(), today.getMonth(), today.getDate() - 7);

    const coachesClientsPerWeek = await Rendezvous.aggregate([
      {
        $match: {
          date: { $gte: oneWeekAgo }, // Consider only appointments in the last week
          status: true // Consider only accepted appointments
        }
      },
      {
        $group: {
          _id: "$coach",
          coachName: { $first: "$coachName" },
          clientsCount: { $sum: { $cond: { if: { $ne: ["$visiteur", null] }, then: 1, else: 0 } } }
        }
      },
      {
        $sort: { clientsCount: -1 } // Sort by clients count in descending order
      }
    ]);

    res.json(coachesClientsPerWeek);
  } catch (error) {
    console.error('Error fetching most clients per week:', error);
    res.status(500).send('Internal Server Error');
  }
});

module.exports = router;
