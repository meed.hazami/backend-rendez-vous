const express = require('express');
const router = express.Router();
const Pack = require('../models/Pack');

router.get('/api/stats/packs-over-time', async (req, res) => {
  try {
    const packsOverTime = await Pack.aggregate([
      {
        $group: {
          _id: { $dateToString: { format: "%Y-%m-%d", date: "$createdAt" } },
          count: { $sum: 1 }
        }
      }
    ]);
    res.json(packsOverTime);
  } catch (error) {
    console.error('Error fetching packs over time:', error);
    res.status(500).send('Internal Server Error');
  }
});

module.exports = router;
