const express = require('express');
const router = express.Router();
const Rendezvous = require('../models/Rendezvous');

router.get('/api/stats/appointments-per-day', async (req, res) => {
  try {
      const appointmentsPerDay = await Rendezvous.aggregate([
          {
              $group: {
                  _id: "$date",
                  total: { $sum: 1 }
              }
          },
          {
              $sort: { _id: 1 }
          }
      ]);
      res.json(appointmentsPerDay);
  } catch (err) {
      console.error(err);
      res.status(500).send('Server Error');
  }
});

module.exports = router;
