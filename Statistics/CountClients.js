const express = require('express');
const router = express.Router();
const client = require('../models/Clients');

router.get('/api/stats/countClient', async (req, res) => {
  try {
    const Clientcount = await client.countDocuments();
    res.json({ count: Clientcount });
  } catch (error) {
    console.error('Error fetching coaches count:', error);
    res.status(500).send('Internal Server Error');
  }
});

module.exports = router;
