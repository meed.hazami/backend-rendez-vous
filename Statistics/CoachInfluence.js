const express = require('express');
const router = express.Router();
const Rendezvous = require('../models/Rendezvous');

router.get('/api/stats/coaches-influence', async (req, res) => {
  try {
    // Aggregate appointments grouped by coach and count the number of visitors who signed up
    const coachesInfluence = await Rendezvous.aggregate([
      {
        $match: { status: true } // Consider only accepted appointments
      },
      {
        $group: {
          _id: "$coach",
          coachName: { $first: "$coachName" },
          appointments: { $sum: 1 },
          visitorsSignedUp: { $sum: { $cond: { if: { $ne: ["$visiteur", null] }, then: 1, else: 0 } } }
        }
      },
      {
        $sort: { visitorsSignedUp: -1 } // Sort by visitors signed up in descending order
      }
    ]);
    res.json(coachesInfluence);
  } catch (error) {
    console.error('Error fetching coaches influence:', error);
    res.status(500).send('Internal Server Error');
  }
});

module.exports = router;
