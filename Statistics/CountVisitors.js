const express = require('express');
const router = express.Router();
const visiteur = require('../models/Visiteur');

router.get('/api/stats/countVisiteur', async (req, res) => {
  try {
    const visiteurCount = await visiteur.countDocuments();
    res.json({ count: visiteurCount });
  } catch (error) {
    console.error('Error fetching coaches count:', error);
    res.status(500).send('Internal Server Error');
  }
});

module.exports = router;
