const express = require('express');
const router = express.Router();
const RendezVous = require('../models/Rendezvous'); // Import the RendezVous model

router.get('/api/sources', async (req, res) => {
  try {
    const result = await RendezVous.aggregate([
      {
        $group: {
          _id: '$source',
          count: { $sum: 1 },
        },
      },
    ]);

    res.json(result);
  } catch (error) {
    res.status(500).json({ message: 'Error fetching data', error });
  }
});

module.exports = router;
