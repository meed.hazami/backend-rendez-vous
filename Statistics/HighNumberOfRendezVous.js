
const express = require('express');
const router = express.Router();
const RendezVous = require('../models/Rendezvous');

router.get('/api/stats/most-coach-per-month', async (req, res) => {
  try {
    const result = await RendezVous.aggregate([
      {
        $group: {
          _id: { month: { $month: "$date" }, year: { $year: "$date" }, coach: "$coach" },
          count: { $sum: 1 }
        }
      },
      {
        $sort: { count: -1 }
      },
      {
        $group: {
          _id: { month: "$_id.month", year: "$_id.year" },
          coach: { $first: "$_id.coach" },
          count: { $first: "$count" }
        }
      },
      {
        $lookup: {
          from: "coaches", // Replace with your actual collection name if different
          localField: "coach",
          foreignField: "_id",
          as: "coach"
        }
      },
      {
        $unwind: "$coach"
      },
      {
        $project: {
          month: "$_id.month",
          year: "$_id.year",
          coachName: "$coach.name",
          count: 1
        }
      },
      {
        $sort: { year: 1, month: 1 }
      }
    ]);

    res.json(result);
  } catch (error) {
    console.error('Error fetching most coach per month:', error);
    res.status(500).send('Internal Server Error');
  }
});

module.exports = router;
