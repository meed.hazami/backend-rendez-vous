const express = require('express');
const router = express.Router();
const Coach = require('../models/Coaches');

router.get('/api/stats/total-coaches', async (req, res) => {
  try {
    const coachesCount = await Coach.countDocuments();
    res.json({ count: coachesCount });
  } catch (error) {
    console.error('Error fetching coaches count:', error);
    res.status(500).send('Internal Server Error');
  }
});

module.exports = router;
