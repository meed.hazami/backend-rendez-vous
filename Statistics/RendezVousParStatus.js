const express = require('express');
const router = express.Router();
const Rendezvous = require('../models/Rendezvous');

router.get('/api/stats/reservations-by-status', async (req, res) => {
  try {
    const reservationsByStatus = await Rendezvous.aggregate([
      {
        $group: {
          _id: "$status",
          count: { $sum: 1 }
        }
      }
    ]);
    res.json(reservationsByStatus);
  } catch (error) {
    console.error('Error fetching reservations by status:', error);
    res.status(500).send('Internal Server Error');
  }
});

module.exports = router;
