const express = require('express');
const router = express.Router();
const Visiteur = require('../models/Visiteur'); // Ensure this path is correct

router.get('/api/visitorsAge', async (req, res) => {
    try {
      const visitorsByAge = await Visiteur.aggregate([
        { $group: { _id: '$age', count: { $sum: 1 } } },
        { $sort: { _id: 1 } }
      ]);
      res.json(visitorsByAge);
    } catch (error) {
      console.error('Error fetching visitors by age:', error);
      res.status(500).send('Internal Server Error');
    }
  });
  
module.exports = router;
