
const express = require('express');
const router = express.Router();
const Rendezvous = require('../models/Rendezvous');

router.get('/api/stats/most-clients-per-month', async (req, res) => {
    try {
      const today = new Date();
      const firstDayOfMonth = new Date(today.getFullYear(), today.getMonth(), 1);
  
      const coachesClientsPerMonth = await Rendezvous.aggregate([
        {
          $match: {
            date: { $gte: firstDayOfMonth }, // Consider only appointments in the current month
            status: true // Consider only accepted appointments
          }
        },
        {
          $group: {
            _id: "$coach",
            coachName: { $first: "$coachName" },
            clientsCount: { $sum: { $cond: { if: { $ne: ["$visiteur", null] }, then: 1, else: 0 } } }
          }
        },
        {
          $sort: { clientsCount: -1 } // Sort by clients count in descending order
        }
      ]);
  
      res.json(coachesClientsPerMonth);
    } catch (error) {
      console.error('Error fetching most clients per month:', error);
      res.status(500).send('Internal Server Error');
    }
  });
  
  module.exports = router;
  