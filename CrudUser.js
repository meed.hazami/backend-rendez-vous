const express = require('express');
const router = express.Router();
const User = require('./models/user');

router.get('/api/users', async (req, res) => {
    try {
        const users = await User.find();
        res.json(users);
    } catch (error) {
        res.status(500).json({ message: error.message });
    }
});

router.get('/api/users/:id', async (req, res) => {
    try {
        const id = req.params.id;
        const user = await User.findById(id);
        if (!user) {
            return res.status(404).json({ message: "User not found" });
        }
        res.json(user);
    } catch (error) {
        res.status(500).json({ message: error.message });
    }
});

router.put('/api/users/:id', async (req, res) => {
    try {
        const { username, email, phone } = req.body;
        const id = req.params.id;

        const updatedUser = await User.findByIdAndUpdate(
            id,
            { username, email, phone },
            { new: true }
        );

        if (!updatedUser) {
            return res.status(404).json({ message: "User not found" });
        }
        res.status(200).json('User updated successfully');
    } catch (error) {
        res.status(500).json({ message: error.message });
    }
});

router.delete('/api/users/:id', async (req, res) => {
    try {
        const id = req.params.id;
        const user = await User.findById(id);
        if (!user) {
            return res.status(404).json({ message: "User not found" });
        }
        await User.findByIdAndDelete(id);
        res.status(200).json('User has been deleted!');
    } catch (error) {
        res.status(500).json({ message: error.message });
    }
});

module.exports = router;
