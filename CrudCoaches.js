const express = require('express');
const Coach = require('./models/Coaches'); // Assuming your Coach model file is named Coach.js


  const Crouter = express.Router();

  // Create a new coach
  Crouter.post('/coaches', async (req, res) => {
    try {
      const { name, status,type_sport,sexe } = req.body;
      const coach = new Coach({ name, status,type_sport,sexe });
      await coach.save();
      res.status(201).json(coach);
    } catch (err) {
      res.status(400).json({ error: err.message });
    }
  });
 

// Define a route to fetch all coaches
Crouter.get('/coaches', async (req, res) => {
  try {
    // Fetch all coaches from the database
    const coaches = await Coach.find();
    res.json(coaches);
  } catch (error) {
    console.error('Error fetching coaches:', error);
    res.status(500).send('Internal Server Error');
  }
});

Crouter.get('/coaches/:id', async (req, res) => {
  try {
    const id = req.params.id 
    console.log("looking for Coach :", id);
    const coach = await Coach.findById(id);
    console.log("Found Coach:", coach); // Log the found pack
    if (coach) {
      res.json(coach);
    } else {
      console.log("Coach not found");
      res.status(404).json({ message: "Coach not found" });
    }
  } catch (error) {
    console.error("Error finding Coach:", error); // Log any errors
    res.status(500).json({ message: error.message });
  }
});


  // Update a coach by ID
  Crouter.put('/coaches/:id', async (req, res) => {
    try {
      const { name, status,type_sport,sexe } = req.body;
      const coach = await Coach.findByIdAndUpdate(req.params.id, { name, status,type_sport,sexe }, { new: true });
      if (!coach) {
        return res.status(404).json({ error: 'Coach not found' });
      }
      res.json(coach);
    } catch (err) {
      res.status(400).json({ error: err.message });
    }
  });

  // Delete a coach by ID
  Crouter.delete('/coaches/:id', async (req, res) => {
    try {
      const coach = await Coach.findByIdAndDelete(req.params.id);
      if (!coach) {
        return res.status(404).json({ error: 'Coach not found' });
      }
      res.json({ message: 'Coach deleted successfully' });
    } catch (err) {
      res.status(500).json({ error: err.message });
    }
  });

  module.exports = Crouter;

